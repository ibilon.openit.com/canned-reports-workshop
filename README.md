# Canned Reports Workshop

## Pre-requisites

1. Install Core Server 9.17.410
1. Install [Python 3.8](https://www.python.org/ftp/python/3.8.8/python-3.8.8-amd64.exe) or 3.9 only (not 3.10)
1. Install [PyOpeniT](https://gitlab.com/openitdotcom/pyopenit). Open command prompt and paste the following:

    ```sh
    pip install git+https://gitlab+deploy-token-576170:Vyd57_bEuGUGj_-3wrqb@gitlab.com/openitdotcom/pyopenit.git#egg=pyopenit
    ```

1. Go to https://gitlab.com/openitdotcom/queryengine and follow the instructions (no need to run)
1. Go to https://gitlab.com/openitdotcom/streamlit and follow the instructions (no need to run)
1. Clone the queries

    ```sh
    git clone git@gitlab.com:openitdotcom/queries.git
    ```

1. Download `\\mnlnas\global_share\data` `QueryEngine Demo Database.zip`
1. Extract to `C:\ProgramData\OpeniT\Data\database`
1. In Core Reporter, turn on Admin -> Configuration -> Registry -> Canned Reports
1. Check `openit_queryengine` and `openit_queryaccelerator` is running
1. Wait for a while until you see the CPU usage of `openit_queryaccelerator` rise
1. Wait until the CPU usage of `openit_queryaccelerator` drops to 0
1. Explore `C:\Program Files\OpeniT\Core\Log\temp\QueryAccelerator`
1. The datatypes inside `Data\database` and `QueryAccelerator\database` should be identical
1. In the browser, open http://localhost:8080/streamlit/
1. Select report: `Heatmap`
1. Select product: `Sentinel [cph_openitsrv9;sentinel]`
1. Select feature: `S3GRAF`
1. Select month: `2015/12/01`
1. Wait until the heatmap appears
1. Explore _License Efficiency_ and _Utilization Trend_ also
